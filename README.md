Задание (вариант 9):

Сайт поиска авиабилетов, в котором есть следующие возможности:

- регистрация пользователей
- иерархия доступа (админ, пользователь)
- управление (CRUD) авиабилетов в админке
- поиск авиабилетов на сайте (по дате, по авиакомпании, по направлению)
- рейтинг авиакомпаний
- поиск по аэропорту

В реализации используются:
1С:Предприятие 8.2

Проверялось в Google Chrome (Версия 40.0.2214.115 m)

Для запуска необходимо иметь установленного клиента 1С:Предприятия версии не старше 8.2.19.83. 
1) Файл .dt загрузить в конфигураторе через меню "Сервис". 
2) Произвести обновление и реструктуризацию базы данных. 
3) Запустить клиента.

Деплой:
git clone https://Ilkor7@bitbucket.org/Ilkor7/mai.git